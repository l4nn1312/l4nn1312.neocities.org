---
title: "Zelda, COVID-19, and State Repression"
date: 2020-08-05T04:23:17-07:00
draft: false
---

Welcome back to my little digital garden. Do make yourself comfortable!
For the summer, I’ve been back in the dumpster hole known as Orange County, and the adjustment has been taking a tole on my mental health. Luckily, my friends have been there for me during these times as well as my trusty Nintendo Switch loaded up with Zelda Breath of the Wild, Pokémon Shield, and Animal Crossing. These forms of escapism have kept me grounded in an otherwise less-than-savory situation. Being able to explore Hyrule, the Gala region, and my little island are worthy alternatives to the sublime Santa Cruz and being able to actually go outside for an extended period of time.

These forms of escapism have also been invaluable in helping me deal with another issue: witnessing the horrors of state repression against my fellow humans fighting for a better future for Black lives, and, more broadly, all of us who are repressed by the state. While I, unfortunately, have been unable as of yet to participate in Black Lives Matter protests (though I hopefully will soon), merely seeing the level of state violence in places like Portland, Seattle, and Chicago have chilled me to the bone. While I know that this sort of police brutality and the violence inherent in the system is nothing new (by far), seeing it day after day, I admit, has taken something of a toll on my mental health. My entire heart goes out to the wonderful people who are out there on the front lines and my sincerest apologies that I’ve been unable as of yet to join them. It is my hope that some real change comes out of this, and the fact that Seattle and Minneapolis have voted to abolish their police departments lends necessary credence to that hope.

Good luck to everyone out there during this pandemic and uprising and stay as safe as you can.
