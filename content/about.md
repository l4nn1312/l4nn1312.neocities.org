---
title: "About"
date: 2020-08-05T08:26:53-07:00
draft: false
menu: "main"
weight: 50
---

Transfeminine futch pan lesbian genderfluid autistic & ADHD kid, CPTSD & OCD, economics and math combined major, music fan, open source advocate, hacker, deer & cat furry, faekin/ robotkin/alienkin, ancom/ anarchosyndicalist/ democraticconfederalist, tech/hedge witch, IWW delegate IU620 IWW, cyberfolk punk. Irish and Juaneñx/Acjachemen, among other things.

Links:
    - [Mastodon](https://hackers.town/@l4nn1312)
    - [Git](https://git.disroot.org/)
